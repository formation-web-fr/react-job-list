import React from "react";
import Tag from "../../components/tags/tags.component"
import "./card.style.css";

export function Card({
  logo,
  company,
  featured,
  new: nouveau,
  position,
  role,
  level,
  postedAt,
  contract,
  location,
  languages,
  tools,
}) {
  const skills = [role, level, ...languages, ...tools];
   
  return (
    <div className="card-container">
      <img src={logo} alt="" />
      <div className="about">
        <p className="company">{company}</p>
        <p>{featured}</p>
        <p>{nouveau}</p>
        <h3>{position}</h3>
        <div className="infos">
          <p>{`${postedAt}  - ${contract} - ${location}`} </p>
        </div>
      </div>

      <div className="skills">
        {skills.map((text, index) => (
          <Tag key={index} text={text} />
        ))} 
      </div>
    </div>
  );
}