import React from "react";
import "./tags.style.css"

export default function tag(props) {
    return (
        <div className="tag">
            { props.text }
        </div>
    )
}