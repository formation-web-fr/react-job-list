import React from "react";
import { Link } from "react-router-dom";
import "./header.style.css";

export const Header = () => (
  <header className="header">
    <Link className="header-link" to="/">
      Home
    </Link>
    <Link className="header-link" to="/Archive">
      Archive
    </Link>
  </header>
);
