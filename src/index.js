import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./index.css";
import App from "./pages/homepage/homepage.component";
import Archive from "./pages/archive/archive.component.jsx"
import {Header} from "./components/header/header.component.jsx"
import { Provider } from 'react-redux'
import { store } from './state/store'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <Header />
        <Routes>
          <Route path="/" element={<App />} />
          <Route path="/archive" element={<Archive />} />
        </Routes>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
