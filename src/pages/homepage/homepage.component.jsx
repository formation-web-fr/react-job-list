import React, { Component } from "react";
import './homepage.css';
import { Link } from 'react-router-dom'
//store
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actionCreators } from '../../state/index'

function App () {
  const state = useSelector((state) => state)
  console.log(state)
  const dispatch = useDispatch()

  const { depositMoney, withdrawMoney } = bindActionCreators(actionCreators, dispatch)
  console.log(depositMoney)

  return (
    <div className="App">
      <h1>Ma page d'acceuil</h1>
      <Link to='/archive'><button>Profils</button></Link>

      <section className="bank">
        <h2 className="bank-title">Mon compte en banque</h2>

        <p className="bank-account"> { state.account }€ </p>

        <button onClick={ () => depositMoney(50)}>Deposer 50€</button>
        <button onClick={ () => withdrawMoney(50)}>Retirer 50€</button>
      </section>
    </div>
  )
}

export default App;
