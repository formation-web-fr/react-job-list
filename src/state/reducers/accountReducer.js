const accountReducer = (state = 0, action) => {
    switch (action.type) {
        case "deposit":
            return state + action.payload
        case "withDraw":
            return state - action.payload
        default:
            return state
    }
}

export default accountReducer